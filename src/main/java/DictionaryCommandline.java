import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

import java.util.List;
import java.util.Scanner;

public class DictionaryCommandline extends DictionaryManagement {
    public static void showAllWords() {
        System.out.printf("%-4s| %-15s| %-15s%n", "No", "English", "Vietnamese");
        int no = 0;
        for (Word word : wordList) {
            no++;
            System.out.printf("%-4d| %-15s| %-15s%n", no, word.getWord_target(), word.getWord_explain());
        }
    }

    public static void dictionarySearcher() {
        System.out.print("Search for : ");
        Scanner scanner = new Scanner(System.in);
        String wordFind = scanner.nextLine();
        int no = 0;
        for (Word word : wordList) {
            if (word.getWord_target().contains(wordFind)) {
                no++;
                System.out.printf("%-4d| %-15s| %-15s%n", no, word.getWord_target(), word.getWord_explain());
            }
        }
        if (no == 0) {
            System.out.println("Cant find word");
        }
    }
    public static void dictionaryBasic() {
        insertFromCommandline();
        showAllWords();
    }
    public static void dictionaryAdvanced()
    {
        insertFromFile();
        showAllWords();
        dictionaryLookup();
        dictionaryDelete();
        dictionaryEdit();
        showAllWords();

        dictionaryExportToFile();
    }
    public static List<Word> getWordList() {
        insertFromFile();
        return wordList;
    }
    public static void speak(String text)
    {
        System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
        Voice voice= VoiceManager.getInstance().getVoice("kevin16");
        voice.allocate();
        voice.speak(text);
    }
}
