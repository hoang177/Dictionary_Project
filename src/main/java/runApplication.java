import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;


public class runApplication {
    private JFrame mainFrame;
    private JPanel searchPanel;
    private JPanel otherPanel;
    private JScrollPane targetWordPane;
    final DefaultListModel wordName = new DefaultListModel();
    private JList targetWordList = new JList();
    private JPanel explainWordPanel;
    JLabel explainWord=new JLabel("",JLabel.CENTER);
    /*
       add
     */
    private JFrame frame;
    private JLabel headerLabel;
    private JPanel controlPanel;

    /*
        delete
     */
    private JFrame frame1;
    private JLabel headerLabel1;
    private JPanel controlPanel1;

    /*
     * change
     */
    private JFrame frame2;
    private JLabel headerLabel2;
    private JPanel controlPanel2;


    List<Word> wordList = DictionaryCommandline.getWordList();
    String searchWord = "";
    public final int width = 400, height = 400;

    public runApplication() {
        prepareGUI();
    }

    private void prepareGUI() {
        setSearchPanel();
        setTargetWordPane();
        setExplainWordPanel();
        setOtherPanel();
        setMainFrame();
        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
    }

    public void setSearchPanel() {
        searchPanel = new JPanel();
        searchPanel.setBackground(Color.blue);
        searchPanel.setLayout(new FlowLayout());
        final JTextField textArea = new JTextField(10);
        JButton searchButton = new JButton("search");
        searchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                searchWord = textArea.getText();
                wordName.removeAllElements();
                for (int i = 0; i < wordList.size(); i++) {
                    if (wordList.get(i).getWord_target().contains(searchWord)) {
                        wordName.addElement(wordList.get(i).getWord_target());
                    }
                }
            }
        });
        JScrollPane scrollPane = new JScrollPane(textArea,
                JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        searchPanel.add(scrollPane);
        searchPanel.add(searchButton);
    }

    public void setOtherPanel() {
        otherPanel = new JPanel();
        otherPanel.setBackground(Color.BLUE);
        otherPanel.setLayout(new FlowLayout());
        JButton add = new JButton("add");
        JButton change = new JButton(("change"));
        JButton delete = new JButton("delete");
        otherPanel.add(add);
        otherPanel.add(change);
        otherPanel.add(delete);

        /*
         * new frame add.
         */
        final JFrame frame = new JFrame("ADD A WORD");
        frame.setSize(300, 200);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridLayout(2, 1));

        headerLabel = new JLabel("", JLabel.CENTER);
        controlPanel = new JPanel();
        controlPanel.setLayout(new FlowLayout());
        frame.add(headerLabel);
        frame.add(controlPanel);

        headerLabel.setText("Press your word and word 's meaning");
        JLabel wordLabel = new JLabel("Word: ", JLabel.RIGHT);
        JLabel meaningLabel = new JLabel("Meaning: ", JLabel.CENTER);
        final JTextField wordText = new JTextField(6);
        final JTextField meaningText = new JTextField(6);
        JButton addButton = new JButton("ADD");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                wordList.add(new Word(wordText.getText(), meaningText.getText()));
                wordName.addElement(wordText.getText());
                frame.dispose();
                //System.out.println(wordList.get(wordList.size()-1).getWord_target());
            }
        });


        controlPanel.add(wordLabel);
        controlPanel.add(wordText);
        controlPanel.add(meaningLabel);
        controlPanel.add(meaningText);
        controlPanel.add(addButton);

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                frame.setVisible(true);
            }
        });


        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setVisible(true);
            }
        });

        /*
         * new frame delete.
         */

        final JFrame frame1 = new JFrame("Delete a word");
        frame1.setSize(300, 200);
        frame1.setLocationRelativeTo(null);
        frame1.setLayout(new GridLayout(2, 0));

        headerLabel1 = new JLabel("", JLabel.CENTER);
        controlPanel1 = new JPanel();
        controlPanel1.setLayout(new FlowLayout());
        frame1.add(headerLabel1);
        frame1.add(controlPanel1);

        JButton deleteButton = new JButton("YES");
        JButton no = new JButton("NO");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                for (int i = 0; i < wordList.size(); i++) {
                    if (wordList.get(i).getWord_target().contains("" + targetWordList.getSelectedValue())) {
                        wordList.remove(i);
                        explainWord.setText("Bấm dịch sau khi đã chọn từ");
                    }
                }
                wordName.removeElement(targetWordList.getSelectedValue());
                frame1.dispose();
            }
        });
        no.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame1.dispose();
            }
        });
        controlPanel1.add(deleteButton);
        controlPanel1.add(no);
        frame1.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                frame1.setVisible(true);
            }
        });

        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                headerLabel1.setText("Delete word " + targetWordList.getSelectedValue() + " ?");
                frame1.setVisible(true);
            }
        });


        /*
         * new frame change
         */
        final JFrame frame2 = new JFrame("Change");
        frame2.setSize(300, 200);
        frame2.setLocationRelativeTo(null);
        frame2.setLayout(new GridLayout(2, 1));

        headerLabel2 = new JLabel("", JLabel.CENTER);
        controlPanel2 = new JPanel();
        controlPanel2.setLayout(new FlowLayout());
        frame2.add(headerLabel2);
        frame2.add(controlPanel2);

        headerLabel2.setText("Change your word");
        JLabel changedWordLabel = new JLabel("Word: ", JLabel.RIGHT);
        JLabel changedMeaningLabel = new JLabel("Meaning: ", JLabel.CENTER);
        final JTextField changedWordText = new JTextField(6);
        final JTextField changedMeaningText = new JTextField(6);
        JButton changeButton = new JButton("CHANGE");
        changeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i = 0; i < wordList.size(); i++) {
                    if (wordList.get(i).getWord_target().contains(""+targetWordList.getSelectedValue())) {
                        wordList.remove(i);
                        wordList.add(i, new Word(changedWordText.getText(), changedMeaningText.getText()));
                        wordName.removeElement(targetWordList.getSelectedValue());
                        wordName.add(i, changedWordText.getText());
                        explainWord.setText(changedWordText.getText()+"          "+changedMeaningText.getText());
                    }
                }
                frame2.dispose();
            }
        });

        controlPanel2.add(changedWordLabel);
        controlPanel2.add(changedWordText);
        controlPanel2.add(changedMeaningLabel);
        controlPanel2.add(changedMeaningText);
        controlPanel2.add(changeButton);


        frame2.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                frame2.setVisible(true);
            }
        });

        change.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changedWordText.setText(""+targetWordList.getSelectedValue());
                changedMeaningText.setText(""+wordList.get(targetWordList.getSelectedIndex()).getWord_explain());
                frame2.setVisible(true);
            }
        });

    }

    public void setTargetWordPane() {
        for (Word word : wordList) {
            if (word.getWord_target().contains(searchWord)) {
                wordName.addElement(word.getWord_target());
            }
        }
        targetWordList = new JList<>(wordName);
        targetWordList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        targetWordList.setSelectedIndex(0);
        //targetWordList.setVisibleRowCount(3);
        targetWordPane = new JScrollPane(targetWordList);
    }

    public void setExplainWordPanel() {
        explainWordPanel = new JPanel();
        explainWordPanel.setLayout(new BorderLayout());
        explainWordPanel.setBackground(Color.CYAN);
        JPanel lane = new JPanel();
        lane.setLayout(new FlowLayout());
        explainWord.setText("Bấm dịch sau khi đã chọn từ");
        JButton jButton = new JButton("Dịch");
        jButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (targetWordList.getSelectedIndex() != -1) {
                    for (Word word : wordList) {
                        if (word.getWord_target().equals(targetWordList.getSelectedValue())) {
                            explainWord.setText(word.getWord_target()+"          "+word.getWord_explain());
                            break;
                        }
                    }
                }
            }
        });
        JButton speak = new JButton("Phát âm ");
        speak.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DictionaryCommandline.speak(""+targetWordList.getSelectedValue());
            }
        });
        lane.add(jButton);
        lane.add(speak);
        lane.setBackground(Color.CYAN);
        explainWordPanel.add(lane, BorderLayout.NORTH);
        explainWordPanel.add(explainWord, BorderLayout.CENTER);
    }

    void setMainFrame() {
        mainFrame = new JFrame("Từ điển");
        mainFrame.setSize(width, height);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.weighty = 0;
        gbc.ipady = width / 16;
        gbc.ipadx = height / 2;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridx = 0;
        gbc.gridy = 0;
        mainFrame.add(searchPanel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        mainFrame.add(otherPanel, gbc);

        gbc.weighty = 1;
        gbc.ipady = height / 16 * 15;
        gbc.gridx = 0;
        gbc.gridy = 1;
        mainFrame.add(targetWordPane, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        mainFrame.add(explainWordPanel, gbc);
    }

    public void run() {
        mainFrame.setVisible(true);
    }
}