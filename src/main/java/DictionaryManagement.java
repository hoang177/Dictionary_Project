import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class DictionaryManagement extends Dictionary {

    public static void insertFromCommandline() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Number of words:");
        int number = scanner.nextInt();
        String a = scanner.nextLine();
        for (int i = 0; i < number; i++) {
            System.out.print("New word no." + (i + 1) + ": ");
            String word_target = scanner.nextLine();
            System.out.print("Explanation in Vietnamese: ");
            String word_explain = scanner.nextLine();
            wordList.add(new Word(word_target, word_explain));
        }
    }

    public static void insertFromFile() {
        try {
            Scanner scanner = new Scanner(new File("dictionaries.txt"));
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                String[] splitWords = line.split("\t");
                String word_target = splitWords[0];
                String word_explain = splitWords[1];
                wordList.add(new Word(word_target, word_explain));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void dictionaryLookup() {
        System.out.print("Look up: ");
        Scanner scanner = new Scanner(System.in);
        String wordFind = scanner.nextLine();
        int no = 0;
        for (Word word : wordList) {
            if (word.getWord_target().equals(wordFind)) {
                no++;
                System.out.printf("%-4d| %-15s| %-15s%n", no, word.getWord_target(), word.getWord_explain());
            }
        }
        if (no == 0) {
            System.out.println("Cant find word");
        }
    }

    public static void dictionaryEdit() {
        System.out.print("Search for editing: ");
        Scanner scanner = new Scanner(System.in);
        String neededWord = scanner.next();
        scanner.nextLine();
        System.out.print("Meaning: ");
        String meaning = scanner.nextLine();

        wordList.add(new Word(neededWord, meaning));
    }

    public static void dictionaryDelete() {
        System.out.print("Search for deleting: ");
        Scanner scanner = new Scanner(System.in);
        String deleteWord = scanner.nextLine();
        int count = 0;
        for (int i = 0; i < wordList.size(); i++) {
            if (wordList.get(i).getWord_target().equals(deleteWord)) {
                    wordList.remove(i);
                    count ++;
                }
        }if (count == 0) {
            System.out.println("No matched word found!");
        }
    }

    public static void dictionaryExportToFile() {
        try {
            System.out.println("Enter file name: ");
            Scanner scanner = new Scanner(System.in);
            String fileName = scanner.next();
            File exportedDict = new File(fileName);

            if (exportedDict.createNewFile()) {
                FileWriter fileWriter = new FileWriter(exportedDict);
                for(Word word : wordList) {
                    fileWriter.write(word.getWord_target() + "\t" + word.getWord_explain() + "\n");
                }
                fileWriter.close();
            } else {
                System.out.println("Can not create new file! File already exists!");
            }
        } catch(IOException exception) {
            System.out.println("An error occurred!");
            exception.printStackTrace();
        }
    }

}


